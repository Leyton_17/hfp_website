<?php 
		
	require_once "Postmanager/DatabaseConnection.php";

	use database\DatabaseConnection;

	class PostManager {

		private static $instance;
		private $database;


	public function likeButton() {
		if($_POST['like']) {
		$like = $this->database->query("UPDATE table set `likes` = `likes`+1 where `id` = '1'");
		$like_result = query($like);
	}
}
		public static function getInstance() {
			if (!self::$instance) {
				self::$instance = new PostManager();
			}
			return self::$instance;
		}
		
		private function __construct(){
			$this->database = new DatabaseConnection("localhost", "root", "","hfp_web");

			if (!$this->database->connect()) {
				die("Could not connect to database");
			}
		}

		public function getALLPosts() {
			//fetch all posts from the database.
			$result = $this->database->query("SELECT * FROM posts ORDER BY created_at DESC");
			$resultArray = $this->database->toArray($result);
			return $resultArray;
		}
		public function createPost()	{
			if (!isset($_POST["title"]) || empty($_POST["title"])) {
				return "Please write a title";
			}
			$title = $_POST["title"];

			if (strlen($title) > 500) {
				return "title is to long!";
			}
			//Validate license
			$allowedLicenses = array("all-rights-reserved", "cc0", "cc-by", "cc-by-sa", "cc-by-nc", "cc-by-nc-sa", "cc-by-nd", "cc-by-nc-nd");
			if (!isset($_POST["license"]) || !in_array($_POST["license"], $allowedLicenses)) {
				return"Please set a license.";			
			}

			$license = $_POST["license"];

			//validate and ipload files
			if (!isset($_FILES["Image"])) {
				return "You must upload a image file.";
			}

			$tempPath = $_FILES["Image"]["tmp_name"];
			var_dump($_FILES["Image"]);
			if ($_FILES["Image"]["size"] > 20000000000000) {
				return "The uploaded file is too big";
			}

			$allowedTypes = array("image/png", "image/webp");
			$fileinfo = new finfo();
			$mimeType = $fileinfo->file($tempPath, FILEINFO_MIME_TYPE);
			echo $mimeType;
			if (!in_array($mimeType, $allowedTypes)) {
				return "The file must be a PNG or a WEBP file!";
			}

			//Make sure the upload directory exists.
			$uploadDirectory = "posts" . DIRECTORY_SEPARATOR . date("Y.m.d");
			if (!file_exists($uploadDirectory)) {
				mkdir($uploadDirectory, 0777, true);
			}

			$fileDestination = $uploadDirectory . DIRECTORY_SEPARATOR . microtime() . ".". strtolower(pathinfo($_FILES["Image"]["name"], PATHINFO_EXTENSION));

			//Move the uploaded Image to the upload directory
			if (!move_uploaded_file($tempPath, $fileDestination)) {
				return "An error ocurred while saving the image. Please try again later.";
			}

			$thumnailPath = $this->generateThumbnail($fileDestination);

			$result = $this->database->query("INSERT INTO posts(title, copyright, imgpath, thumbnailpath) VALUES (?,?,?,?)", array($title, $license, $fileDestination , $thumnailPath), array("s", "s", "s", "s"));

			if ($result !== true) {
				return "Please provide user token";
			}

			return $this->database->getInsertedId();
	}
	private function generateThumbnail($imaegPath) {
		$imageData = getimagesize($imaegPath);
		$width = $imageData[0];
		$height = $imageData[1];
		$imageType = $imageData[2];

		if ($width <=128) {
			return $imaegPath;
		}
		else if ($imageType == IMAGETYPE_PNG) {
			$image = imagecreatefrompng($imaegPath);
		}
		else if ($imageType == IMAGETYPE_WEBP) {
			$image = imagecreatefromwebp($imaegPath);
		}
		else {
			return $imaegPath;
		}
		$image = imagescale($image, 120);

		$pathinfo = pathinfo($imaegPath);
		$thumbnailPath = $pathinfo["dirname"] . DIRECTORY_SEPARATOR . $pathinfo["filename"] . " (Thumbnail)." . $pathinfo["extension"];

		if ($imageType == IMAGETYPE_PNG) {
			imagepng($image, $thumbnailPath);
		}

		return $thumbnailPath;
	}
}

?>