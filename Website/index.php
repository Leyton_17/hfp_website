<?php
	error_reporting(E_ALL);
	ini_set('display_errors', '1');

?>
<!DOCTYPE html>
<html>
<head>
	<script src="lazyLoading.js"></script>
	<link href="https://fonts.googleapis.com/css2?family=Arya&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="stylesheet.css">
	<link rel="stylesheet" type="text/css" href="header.css">
	<title>Holofan_Pictures</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="apple-touch-icon" sizes="180x180" href="Favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="Favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="Favicon/favicon-16x16.png">
	<link rel="manifest" href="Favicon/site.webmanifest">
	<link rel="mask-icon" href="Favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="Favicon/favicon.ico">
	<meta name="msapplication-TileColor" content="#2b5797">
	<meta name="msapplication-config" content="Favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
</head>
<body>
<div class="topnav">
  	<a class="active" href="index.php"><img src="Pictures\HololiveV_1.webp"/a>
  	<a href="wiH\wiH.php">What is Hololive</a>
  	<a href="upload.php">Upload</a>
  	<a href="Rules\rules.php">Rules</a>
</div>

		<?php
		if (preg_match('~MSIE|Internet Explorer~i', $_SERVER['HTTP_USER_AGENT']) || preg_match('~Trident/7.0(; Touch)?; rv:11.0~', $_SERVER['HTTP_USER_AGENT'])) {
			echo "<h1>This website does not support Internet Explorer. It's time to move on and install a newer browser.</h1>";
			echo "<p><a href=\"https://www.google.com/chrome/\">Get Google Chrome</a></p>";
			echo "<p><a href=\"https://www.mozilla.org/firefox/new/\">Get Mozilla Firefox</a></p>";
			die;
		}

		?>

		<?php require_once "post-list.php" ?>
</body>
</html>