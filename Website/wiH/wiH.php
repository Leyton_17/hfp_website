<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/css2?family=Arya&display=swap" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="wiH.css">
	<title class="HL">What is Hololive</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../Favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../Favicon/favicon-16x16.png">
	<link rel="manifest" href="../Favicon/site.webmanifest">
	<link rel="mask-icon" href="../Favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="../Favicon/favicon.ico">
	<meta name="msapplication-TileColor" content="#2b5797">
	<meta name="msapplication-config" content="../Favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
</head>
<body>
	<div class="topnav">
  	<a class="active" href="index.php"><img src="../Pictures\HololiveV_1.webp"/a>
  	</div>
  	<div>
  	<a href="upload.php">Upload</a>
  	<a href="Rules\rules.php">Rules</a>
	<a href="https://en.hololive.tv/" target="_blank">Hololive</a>
	<article>
		<h1>Description:</h1>
		<p>Cover Co., Ltd. is a production of VTuber with the vision of enthusiasm of fans all over the world by providing the most advanced 2D entertainment experience from Japan by utilizing technologies such as VR, AR, 5G. It is a company that operates and develops VR / AR live distribution systems, AR apps, etc. It is based in Japan and spans over Three branches with diffrent Languages Hololive Production(Japan), Hololive Indonesia, Hololive English Azki Music
		(A music lable) and Holostars (Male branch) as of 2021. In each Branch there are diffrent Generations with 5 Talents(with a few exeptions) for example Hololive English Generation 1 wich has 5 Talents (Mori Calliope, Takanaschi Kiara, Ninomae Ina'nis, Gawr Gura and Amelia Watson as opposed to Hololive production wich has 4 Shisiron Botan, Yukihana Lamy, Momozusu Nene and Omaru Polka) what sets the Company apart from other"Idol Groups" is that their Talents are Represented through Avatars these are Controlled by the talents via Motion Technologie. All Talents have a Youtube Channel and Livestream regularly. In 2020 the Company has seen the biggest growth with many members hitting the 1 Million subscriber Milestone and 7 out of 10 Talents earning the most Money throug donations. All the Talents ar also active on Twitter and there is a official Subreddit with Memes, News, Milestones etc.</p>
	</article>
	</div>
</body>
</html>