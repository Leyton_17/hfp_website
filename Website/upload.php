<?php 

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require_once "Postmanager/PostManager.php";



$creationResult = null;
if (isset($_POST["submitbutton"])) {

	$creationResult = PostManager::getInstance()->createPost();	

}
?>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="header.css">
	<link rel="stylesheet" type="text/css" href="upload.css">
	<link rel="apple-touch-icon" sizes="180x180" href="Favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="Favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="Favicon/favicon-16x16.png">
	<link rel="manifest" href="Favicon/site.webmanifest">
	<link rel="mask-icon" href="Favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="Favicon/favicon.ico">
	<meta name="msapplication-TileColor" content="#2b5797">
	<meta name="msapplication-config" content="Favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<title>Upload</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="header.css">
<div class="topnav">
  	<a class="active" href="index.php"><img src="Pictures/HololiveV_1.webp"></a>
  	<a class="wiH" href="wiH\wiH.php">What is Hololive</a>
  	<a class= "upload"href="Rules\rules.php">Rules</a>
</div>
</head>
	<body>
		<?php if (!is_numeric($creationResult)): ?>
					<form enctype="multipart/form-data" method="POST">
				<input class="Title" type="text" name="title" placeholder="Title">
				<br>
				<input class="Username" type="text" name="username" placeholder="Username">
				<br>
				<input class="Description" type="text" name="description" placeholder="Description">
				<select class="license" name="license">
					<option value="all-rights-reserved" selected>All rights reserved</option>
					<option value="cc-by-nc-nd">CC BY-NC-ND</option>
					<option value="cc-by-nd">CC BY-ND</option>
					<option value="cc-by-nc-sa">CC BY-NC-SA</option>
					<option value="cc-by-nc">CC BY-NC</option>
					<option value="cc-by-sa">CC BY-SA</option>
					<option value="cc-by">CC BY</option>
					<option value="cc0">CC0 / Public Domain</option>
			</select>
			<br>
			<input type="hidden" name="MAX_FILE_SIZE" value="2000000000000000">
			<input class="Image" type="file" name="Image">
			<br>
			<input class="submit" type="Submit" name="submitbutton">
		</form>
	<?php endif; ?>

		<?php if (isset($_POST["submitbutton"])): ?>
			<?php if (is_numeric($creationResult)): ?>
				<p>The post was created successfully. <a href="post.php?id=<?php echo $creationResult; ?>">View post</a></p>
			<?php else: ?>
				<p><?php echo $creationResult; ?></p>
			<?php endif; ?>
		<?php endif; ?>
	</body>
</html>