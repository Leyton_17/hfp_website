<!DOCTYPE html>
<html>
<head>
	<script src="lazyLoading.js"></script>
	<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../Favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../Favicon/favicon-16x16.png">
	<link rel="manifest" href="../Favicon/site.webmanifest">
	<link rel="mask-icon" href="../Favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="../Favicon/favicon.ico">
	<meta name="msapplication-TileColor" content="#2b5797">
	<meta name="msapplication-config" content="../Favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<link href="https://fonts.googleapis.com/css2?family=Arya&display=swap" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="rules.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Rules</title>
</head>
<body>
	<h1>Important</h1>
	<ol start="1">
		<li>No Lewd Pictures!(for now)</li>
		<li>Don't steal from others (select the right <a href="https://creativecommons.org/about/cclicenses/" target="_blank"> creative commons</a> option when uploading a picture)</li>
		<li>Dont use any words that could offend anyone!</li>
	</ol>


</body>
</html>