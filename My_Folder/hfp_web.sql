-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 16. Mrz 2021 um 11:22
-- Server-Version: 10.4.17-MariaDB
-- PHP-Version: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `hfp_web`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `posts`
--

CREATE TABLE `posts` (
  `id` int(15) NOT NULL,
  `copyright` varchar(250) NOT NULL,
  `user` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `title` varchar(200) NOT NULL,
  `imgpath` varchar(300) NOT NULL,
  `thumbnailpath` varchar(300) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `posts`
--

INSERT INTO `posts` (`id`, `copyright`, `user`, `description`, `title`, `imgpath`, `thumbnailpath`, `created_at`, `likes`) VALUES
(2, 'all-rights-reserved', '', '', 'Test', 'posts\\2021.03.15\\0.37934100 1615804602.png', 'posts\\2021.03.15\\0.37934100 1615804602 (Thumbnail).png', '2021-03-15 10:36:42', 0),
(3, 'all-rights-reserved', '', '', 'hallo', 'posts\\2021.03.15\\0.57492100 1615847310.png', 'posts\\2021.03.15\\0.57492100 1615847310 (Thumbnail).png', '2021-03-15 22:28:30', 0),
(4, 'all-rights-reserved', '', '', 'sa', 'posts\\2021.03.16\\0.22567000 1615882880.png', 'posts\\2021.03.16\\0.22567000 1615882880 (Thumbnail).png', '2021-03-16 08:21:20', 0),
(5, 'all-rights-reserved', '', '', 'EWFS', 'posts\\2021.03.16\\0.79480300 1615883207.png', 'posts\\2021.03.16\\0.79480300 1615883207 (Thumbnail).png', '2021-03-16 08:26:48', 0);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `imgpath` (`imgpath`),
  ADD UNIQUE KEY `thumbnailpath` (`thumbnailpath`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
